# mksort.js

The only algorithm, which is the subset of each and every algorithm used in the online world is Sorting. As there are many uses of it, so, it is important to use with best attributes and make it as simple as possible. Today there are more than 1000 algorithms for sorting are <a href="http://amanandvan247.com/">public</a> and many more are joinng each year.
mksort.js is standalone JavaScript module providing SQL "ORDER BY" ASC/DESC sorting
on multiple keys of arrays of objects.

Sorting arrays of uneven objects will be unpredictable if you use any keys missing 
from some objects.

This module was developed specifically to sort .rows arrays of csvjson.js csv2json 
output objects.

demo.html shows a sample use case.

Author: Fil Baumanis ( @unfail, @bitless )